import { RandomisationService } from './randomisation.service';
import { Strata } from './dto/randomisation-parameters.dto';
import * as fs from 'fs';
import { RatiosService } from './ratios.service';

describe('AppController', () => {
  let appService: RandomisationService;

  beforeAll(async () => {
    appService = new RandomisationService(new RatiosService());
  });

  it('should generate test results', () => {
    expect(true).toBe(true);
  });
});
