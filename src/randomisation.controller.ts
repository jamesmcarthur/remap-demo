import { Body, Controller, Get, Post } from '@nestjs/common';
import { RandomisationService } from './randomisation.service';
import { RandomisationResultDto } from './dto/randomisation-result.dto';
import { RandomisationParametersDto } from './dto/randomisation-parameters.dto';

@Controller()
export class RandomisationController {
  constructor(private readonly appService: RandomisationService) {}

  @Post('/randomise')
  randomise(@Body() randomisationParameters: RandomisationParametersDto): RandomisationResultDto {
    return this.appService.randomise(randomisationParameters);
  }
}
