import { Injectable } from '@nestjs/common';
import { RandomisationParametersDto } from './dto/randomisation-parameters.dto';
import { RandomisationResultDto } from './dto/randomisation-result.dto';
import { RatiosService } from './ratios.service';

@Injectable()
export class RandomisationService {
  constructor(private readonly ratiosService: RatiosService) {}

  randomise(parameters: RandomisationParametersDto): RandomisationResultDto {
    // Transform the array of treatments ["A1", "A2", "B1"] => {A: [1,2], B: [1,2]}
    const eligibleTreatments: EligibleTreatmentsByDomain = {};
    parameters.eligibleTreatments.forEach(code => {
      const domain = code[0];
      const treatment = parseInt(code[1]);

      if (eligibleTreatments[domain]) {
        eligibleTreatments[domain].push(treatment);
      } else {
        eligibleTreatments[domain] = [treatment];
      }
    });

    // Filter out regimens the patient is not eligible for
    let availableRatios = this.ratiosService.getRatiosForStrata(parameters.strata);
    Object.keys(eligibleTreatments).forEach(domain => {
      availableRatios = availableRatios.filter(ratio =>
        eligibleTreatments[domain].includes(ratio.regimen.treatments[domain])
      );
    });

    // Sum up the weightings of all of the regimens the patient is eligible for
    const sum = availableRatios.reduce((a, b) => a + b.ratio, 0);

    // Use an accumulator to keep track of where we're up to
    let acc = 0;

    // Random number between 0 and sum
    const random = Math.random() * sum;

    // Each regimen has a window of size "ratio". If the random number falls within this window
    // then that regimen is selected.
    for (const regimen of availableRatios) {
      if (acc <= random && random < acc + regimen.ratio) {
        return {
          regimen: regimen.regimen.toStringWithZeros(Object.keys(eligibleTreatments)),
        };
      }

      acc += regimen.ratio;
    }
  }
}

class EligibleTreatmentsByDomain {
  [x: string]: number[];
}
