const DOMAIN_DISPLAY_ORDER = "AMCIXY";
const sort = (a: string, b: string) => {
  const aIndex = DOMAIN_DISPLAY_ORDER.indexOf(a).toString();
  const bIndex = DOMAIN_DISPLAY_ORDER.indexOf(b).toString();
  return aIndex.localeCompare(bIndex);
};

export class Regimen {
  treatments: Treatments;

  constructor(stringRepresentation: string) {
    this.treatments = {};
    for (let i = 0; i < stringRepresentation.length; i += 2) {
      this.treatments[stringRepresentation[i]] = parseInt(stringRepresentation[i + 1]);
    }
  }

  toString(): string {
    return Object.keys(this.treatments)
      .sort(sort)
      .map(domain => domain + this.treatments[domain])
      .join('');
  }

  toStringWithZeros(eligibleDomains: string[]): string {
    return Object.keys(this.treatments)
      .sort(sort)
      .map(domain => `${domain}${eligibleDomains.includes(domain) ? this.treatments[domain] : 0}`)
      .join('');
  }
}

class Treatments {
  [x: string]: number;
}
