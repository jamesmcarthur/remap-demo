import { ApiProperty } from '@nestjs/swagger';

export class RandomisationParametersDto {
  @ApiProperty({ description: 'Strata the patient is in' })
  strata: Strata;

  @ApiProperty({
    description: 'Array of possible treatments for the patient',
    example: ['A1', 'A2', 'A3', 'X1', 'X2', 'X3', 'X4', 'Y1', 'Y2', 'Y3'],
  })
  eligibleTreatments: string[];
}

export enum Strata {
  Pandemic = 'Pandemic',
  Pandemic20Apr = 'Pandemic20Apr',
  Test = 'Test',
}
