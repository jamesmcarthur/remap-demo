import { Regimen } from './regimen.dto';

export class Ratio {
  readonly regimen: Regimen;
  readonly ratio: number;
}
