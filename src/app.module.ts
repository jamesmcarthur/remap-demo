import { Module } from '@nestjs/common';
import { RandomisationController } from './randomisation.controller';
import { RandomisationService } from './randomisation.service';
import { RatiosService } from './ratios.service';

@Module({
  imports: [],
  controllers: [RandomisationController],
  providers: [RandomisationService, RatiosService],
})
export class AppModule {}
