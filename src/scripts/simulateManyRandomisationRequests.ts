import { RandomisationParametersDto, Strata } from '../dto/randomisation-parameters.dto';
import * as fs from "fs";
import { RandomisationService } from '../randomisation.service';
import { RatiosService } from '../ratios.service';

// const PARAMS = {
//   strata: Strata.Pandemic20Apr,
//   eligibleTreatments: [
//     'A1',
//     'A2',
//     'A3',
//     'A4',
//     'A5',
//     'M0',
//     'M1',
//     'M2',
//     'I1',
//     'I2',
//     'I3',
//     'C1',
//     'C2',
//     'C3',
//     'X1',
//     'X2',
//     'X3',
//     'X4',
//     'Y1',
//     'Y2',
//     'Y3',
//     'Y4',
//     'Y5',
//   ],
// };
const PARAMS: RandomisationParametersDto = {
  strata: Strata.Pandemic,
  eligibleTreatments: ['C1', 'C2', 'C3', 'X1', 'X2', 'X3', 'X4', 'Y1', 'Y2', 'Y3']
};
const ITERATIONS = 10000000;

const ratiosService = new RatiosService();
const randomisationService = new RandomisationService(ratiosService);
const counts = {};

const start = Date.now();
for (let i = 0; i < ITERATIONS; i++) {
  if (i % 1000 === 0) {
    console.log(`${(i/ITERATIONS*100).toFixed(3)}%`);
  }

  const regimen = randomisationService.randomise(PARAMS).regimen;

  if (counts[regimen]) {
    counts[regimen] += 1;
  } else {
    counts[regimen] = 1;
  }
}
const time = Date.now() - start;

console.log(`Test took ${(time / 1000).toFixed(3)}s`);
console.log(`${(time / ITERATIONS).toFixed(3)}ms per test`);

const csv = Object.keys(counts)
  .sort()
  .map(regimen => `${regimen},${counts[regimen]}\n`)
  .join('');
fs.writeFileSync('output1.csv', csv);