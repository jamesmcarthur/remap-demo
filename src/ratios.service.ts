import { Injectable } from '@nestjs/common';
import * as csvParse from 'csv-parse/lib/sync';
import * as fs from 'fs';
import { Strata } from './dto/randomisation-parameters.dto';
import { Regimen } from './dto/regimen.dto';
import { Ratio } from './dto/ratio.dto';

export class Ratios {
  [x: string]: Ratio[];
}

// Load ratios into memory
const ratios: Ratios = {};
for (const strata in Strata) {
  const csv = fs.readFileSync(`src/ratios/${strata}.csv`);
  const rows = csvParse(csv);
  ratios[strata] = rows.map(row => ({
    regimen: new Regimen(row[0]),
    ratio: parseFloat(row[1]),
  }));
}

@Injectable()
export class RatiosService {
  getRatiosForStrata(strata: string): Ratio[] {
    return ratios[strata];
  }
}
