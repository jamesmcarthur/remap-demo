import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import assert = require('assert');

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/randomise (POST)', () => {
    return request(app.getHttpServer())
      .post('/randomise')
      .send({
        "strata": "Pandemic",
        "eligibleTreatments": [
          "A1",
          "A2",
          "A3",
          "X1",
          "X2",
          "X3",
          "X4",
          "Y1",
          "Y2",
          "Y3"
        ]
      })
      .expect(201)
      .then(response => {
        assert(typeof response.body.regimen === "string")
      })
  });
});
